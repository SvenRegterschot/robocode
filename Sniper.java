package nl.saxion;

import robocode.*;
import robocode.util.Utils;
import sampleteam.Point;

import java.awt.*;
import java.awt.geom.Point2D;

import static robocode.util.Utils.normalRelativeAngle;
import static robocode.util.Utils.normalRelativeAngleDegrees;

public class Sniper extends TeamRobot {
    //The middle of our field, we'll divide it into four quadrants.
    static double fieldXMid;
    static double fieldYMid;

    //Enemy's X and Y.
    static double EX;
    static double EY;

    //Our bullet power and turn direction.
    static double maxBP;

    //Our direction
    int dir = 1;

    public void run() {
        // Set colors
        setBodyColor(Color.red);
        setGunColor(Color.black);
        setRadarColor(Color.yellow);
        setBulletColor(Color.green);
        setScanColor(Color.green);
//
        //Find the middle of the field.
        fieldYMid = getBattleFieldHeight() / 2;
        fieldXMid = getBattleFieldWidth() / 2;
        setAdjustGunForRobotTurn(true);
        setAdjustRadarForGunTurn(true);
        do {
            turnRadarRightRadians(Double.POSITIVE_INFINITY);
        } while (true);
    }
        @Override
        public void onMessageReceived(MessageEvent e) {
            if (e.getMessage() instanceof Point) {
                Point p = (Point) e.getMessage();
                // Calculate x and y to target
                double dx = p.getX() - this.getX();
                double dy = p.getY() - this.getY();
                System.out.println(" point is "+dx + "and " + dy);
                // Calculate angle to target
                double theta = Math.toDegrees(Math.atan2(dx, dy));

                // Turn gun to target
                turnGunRight(normalRelativeAngleDegrees(theta - getGunHeading()));
                // Fire hard!
                fire(3);}
    }
    /**
     * onScannedRobot:  fire if its not my team mate.
     */

    public void onScannedRobot(ScannedRobotEvent e) {        if (isTeammate(e.getName())) {
            System.out.println(" is my team.cant kill");
            return;
        } else {

            //Our maximum bulletPower is 3, unless we are further than 200 distance units away from them, in which case it is 2.4.
            //We'll also find our bulletSpeed here.
            double maxBP = 3;
            if (e.getDistance() > 200) {
                maxBP = 2.4;
            }
            double bulletPower = Math.min(maxBP, Math.min(getEnergy() / 10, e.getEnergy() / 4));


            //Find our enemy's absolute bearing and makes a thing used for painting.
            double absBearing = e.getBearingRadians() + getHeadingRadians();

            //This finds our enemies X and Y (note:  it can be used to find the X and Y of anything by switching the distance and bearing).
            EX = getX() + e.getDistance() * Math.sin(absBearing);
            EY = getY() + e.getDistance() * Math.cos(absBearing);


            double predictedX = EX, predictedY = EY;
            //Find the bearing of our predicted coordinates from us.
            double aim = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));

            //Aim and fire.
            setTurnGunRightRadians(Utils.normalRelativeAngle(aim - getGunHeadingRadians()));
            setFire(bulletPower);
            setAhead(100 * dir);
            setTurnRadarRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getRadarHeadingRadians()) * 2);

        }
    }

    /**
     * onBullethit: move Away.
     */
    public void onBulletHit(BulletHitEvent e) {
        ahead(100); //The robot goes away from the enemy.
        turnRight(360);
    }


}


