package nl.saxion;

import robocode.*;
import robocode.util.Utils;
import samplesentry.BorderGuard;
import sampleteam.Point;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Vector;

import static robocode.util.Utils.normalRelativeAngle;
import static robocode.util.Utils.normalRelativeAngleDegrees;

public class Dummy extends TeamRobot {


    int dir = 1;
    boolean peek = false;
    static double EX;
    static double EY;
    RobotData enemy;

    public void run() {

        setBodyColor(Color.YELLOW);
        setGunColor(Color.black);
        setRadarColor(Color.orange);
        setBulletColor(Color.cyan);
        setScanColor(Color.cyan);

        setTurnRadarRight(Double.POSITIVE_INFINITY);


        // turnLeft to face a wall.
        // getHeading() % 90 means the remainder of
        // getHeading() divided by 90.
        turnLeft(getHeading() % 90);


        String[] teammates = getTeammates();
        if (teammates != null) {
            for (String member : teammates) {
                out.println(member);
            }
        }
        try {
            broadcastMessage(teammates);
        } catch (Exception e) {
            e.printStackTrace();
        }
//


        while (true) {
            DirectionMessage dm;
            peek = true;

            if (Utils.isNear(getHeadingRadians(), 0D) || Utils.isNear(getHeadingRadians(), Math.PI)) {
                ahead((Math.max(getBattleFieldHeight() - getY(), getY()) - 28) * dir);

            } else {
                ahead((Math.max(getBattleFieldWidth() - getX(), getX()) - 28) * dir);

            }
            dm = new DirectionMessage(DirectionMessage.RIGHT);
            peek = false;
            // Turn to the next wall
            turnRight(90);


            try {
                broadcastMessage(dm);
                System.out.println("hey we are ready!keep up your spirits");
            } catch (IOException e) {
                e.printStackTrace();
            }
            execute();


        }
    }

    /**
     * onScannedRobot: Fire if the other bot is not my team mate.
     */
    public void onScannedRobot(ScannedRobotEvent e) {
        if (isTeammate(e.getName())) {
            System.out.println("hey team mate!");
            return;
        } else {
            double maxBP = 3;
            if (e.getDistance() > 200) {
                maxBP = 2.4;
            }
            double bulletPower = Math.min(maxBP, Math.min(getEnergy() / 10, e.getEnergy() / 4));


            //Find our enemy's absolute bearing and makes a thing used for painting.
            double absBearing = e.getBearingRadians() + getHeadingRadians();

            //This finds our enemies X and Y (note:  it can be used to find the X and Y of anything by switching the distance and bearing).
            EX = getX() + e.getDistance() * Math.sin(absBearing);
            EY = getY() + e.getDistance() * Math.cos(absBearing);

            // Calculate enemy's position
            double enemyX = getX() + e.getDistance() * Math.sin(Math.toRadians(absBearing));
            double enemyY = getY() + e.getDistance() * Math.cos(Math.toRadians(absBearing));

            try {
                // Send enemy position to teammates
                broadcastMessage(new Point(enemyX, enemyY));
                sendMessage("Sniper", new Point(enemyX, enemyY));
                out.println("message sent: ");

            } catch (IOException ex) {
                out.println("Unable to send order: ");
                ex.printStackTrace(out);
            }


            double predictedX = EX, predictedY = EY;
            //Find the bearing of our predicted coordinates from us.
            double aim = Utils.normalAbsoluteAngle(Math.atan2(predictedX - getX(), predictedY - getY()));

            //Aim and fire.
            setTurnGunRightRadians(Utils.normalRelativeAngle(aim - getGunHeadingRadians()));
            setFire(bulletPower);
            setAhead(100 * dir);
            setTurnRadarRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getRadarHeadingRadians()) * 2);


        }
    }

    /**
     * onBulletHit just move a bit to avoid damage:
     */
    public void onBulletHit(BulletHitEvent e) {
        turnRight(90);
        turnRight(normalRelativeAngleDegrees(90 - (getHeading())));
        ahead(dir);//The robot goes away from the enemy.

    }

    public String[] getTeammates() {
        String[] names = {" Dummy", "Sniper", "Hunter1","Hunter2","Hunter3"};
        return names;
    }
    public void onMessageReceived(MessageEvent e) {

        // Fire at a point
        if (e.getMessage() instanceof Point) {
            Point p = (Point) e.getMessage();

            // Calculate x and y to target
            double dx = p.getX() - this.getX();
            double dy = p.getY() - this.getY();

            // Calculate angle to target
            double theta = Math.toDegrees(Math.atan2(dx, dy));

            // Turn gun to target
            setTurnGunRight(normalRelativeAngleDegrees(theta - getGunHeading()));
        }
    }

    public void onRobotDeath(RobotDeathEvent e) {
        if (enemy.name == e.getName()) {
            enemy.name = "";

        }
    }

    class RobotData {
        String name; // name of the scanned robot
        double scannedX; // x coordinate of the scanned robot based on the last update
        double scannedY; // y coordinate of the scanned robot based on the last update
        double scannedVelocity; // velocity of the scanned robot from the last update
        double scannedHeading; // heading of the scanned robot from the last update
        double targetX; // predicated x coordinate to aim our gun at, when firing at the robot
        double targetY; // predicated y coordinate to aim our gun at, when firing at the robot

        /**
         * Creates a new robot data entry based on new scan data for a scanned robot.
         *
         * @param event is a ScannedRobotEvent event containing data about a scanned robot.
         */
        RobotData(ScannedRobotEvent event) {
            // Store the name of the scanned robot
            name = event.getName();
            // Updates all scanned facts like position, velocity, and heading
            update(event);
            // Initialize the coordinates (x,y) to fire at to the updated scanned position
            targetX = scannedX;
            targetY = scannedY;
        }

        /**
         * Updates the scanned data based on new scan data for a scanned robot.
         *
         * @param event is a ScannedRobotEvent event containing data about a scanned robot.
         */
        void update(ScannedRobotEvent event) {
            // Get the position of the scanned robot based on the ScannedRobotEvent
            Point2D.Double pos = getPosition(event);
            // Store the scanned position (x,y)
            scannedX = pos.x;
            scannedY = pos.y;
            // Store the scanned velocity and heading
            scannedVelocity = event.getVelocity();
            scannedHeading = event.getHeadingRadians();
        }

        /**
         * Returns the position of the scanned robot based on new scan data for a scanned robot.
         *
         * @param event is a ScannedRobotEvent event containing data about a scanned robot.
         * @return the position (x,y) of the scanned robot.
         */
        Point2D.Double getPosition(ScannedRobotEvent event) {
            // Gets the distance to the scanned robot
            double distance = event.getDistance();
            // Calculate the angle to the scanned robot (our robot heading + bearing to scanned
            // robot)
            double angle = getHeadingRadians() + event.getBearingRadians();

            // Calculate the coordinates (x,y) of the scanned robot
            double x = getX() + Math.sin(angle) * distance;
            double y = getY() + Math.cos(angle) * distance;

            // Return the position as a point (x,y)
            return new Point2D.Double(x, y);
        }


    }
}

